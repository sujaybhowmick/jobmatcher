package com.swipejobs.service;

import com.google.gson.Gson;
import com.mashape.unirest.http.JsonNode;
import com.swipejobs.model.Job;
import com.swipejobs.exceptions.ResourceNotFoundException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sbhowmick on 2/28/17.
 */
@Component
public class JobService extends AbstractService {

    @Value("${swipejobs.baseUrl}")
    private String baseUrl;

    @Value("${swipejobs.jobsApi}")
    private String jobsApi;

    private Map<Integer, Job> jobs = new HashMap<>();

    @PostConstruct
    public void loadJobs() {
        final String jobsUrl = baseUrl + jobsApi;

        Gson gson = new Gson();
        JsonNode jsonNode = callRemoteService(jobsUrl);
        JSONArray jsonArray = jsonNode.getArray();
        for (int i = 0; i < jsonArray.length(); i++) {
            JSONObject jsonObject = jsonArray.getJSONObject(i);
            Job job = gson.fromJson(jsonObject.toString(), Job.class);
            jobs.put(job.jobId, job);
        }
    }

    public Map<Integer, Job> getAllJobs() {
        return this.jobs;
    }

    public Job findJobById(Integer id) {
        Job job = jobs.get(id);
        if (job != null) {
            return job;
        }
        throw new ResourceNotFoundException(String.format("Job with id %d does not exists", id));
    }

}
