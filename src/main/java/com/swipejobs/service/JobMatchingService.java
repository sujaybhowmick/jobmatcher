package com.swipejobs.service;

import com.swipejobs.model.Job;
import com.swipejobs.model.JobResult;
import com.swipejobs.model.Location;
import com.swipejobs.model.Worker;
import com.swipejobs.exceptions.ResourceNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * Created by sbhowmick on 2/28/17.
 */
@Component
public class JobMatchingService {

    @Autowired
    private JobService jobService;

    @Autowired
    private WorkerService workerService;

    /**
     * Matching worker to jobs is as follows
     * 1. Worker has at least one of the required certificates for the job
     * 2. The Job requires worker to have a driver license
     * 3. The job location is within the max job distance preferred by the worker
     *
     * @param workerId
     * @return
     */
    public JobResult matchJobsForWorker(Integer workerId) {
        Map<Integer, Job> allJobs = jobService.getAllJobs();
        Worker worker = workerService.findWorkerById(workerId);
        if (worker != null) {

            List<Job> matchedJobs = allJobs.entrySet().stream()
                    .map(x -> x.getValue())
                    .filter(job ->
                            job.requiredCertificates.stream().filter(r -> worker.certificates.contains(r)).count() > 0 &&
                                    job.driverLicenseRequired == worker.hasDriversLicense &&
                                    job.location.distance(new Location()
                                            .withLatitude(worker.jobSearchAddress.latitude)
                                            .withLongitude(worker.jobSearchAddress.longitude), "K")
                                            <= worker.jobSearchAddress.maxJobDistance)
                    .collect(Collectors.toList());
            return new JobResult(worker, matchedJobs);
        }
        throw new ResourceNotFoundException(String.format("Worker with id %d does not exists", workerId));
    }
}
