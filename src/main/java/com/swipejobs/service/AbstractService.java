package com.swipejobs.service;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.swipejobs.exceptions.RestServiceCallException;
import com.swipejobs.exceptions.ServiceException;
import com.swipejobs.exceptions.ServiceTimedOutException;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

/**
 * Created by sbhowmick on 2/28/17.
 */
public abstract class AbstractService {

    public JsonNode callRemoteService(String url) {
        Future<HttpResponse<JsonNode>> futureResponse = Unirest.get(url).asJsonAsync();
        try {

            HttpResponse<JsonNode> response = futureResponse.get(5L, TimeUnit.SECONDS);
            if (response.getStatus() == 200) {
                return response.getBody();
            } else {
                throw new RestServiceCallException(String.format(response.getStatusText() + ": %d", response.getStatus()));
            }
        } catch (InterruptedException | ExecutionException e) {
            throw new ServiceException(e);
        } catch (TimeoutException e) {
            throw new ServiceTimedOutException(e);
        }
    }

}
