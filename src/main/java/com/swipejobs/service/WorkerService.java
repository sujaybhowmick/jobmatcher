package com.swipejobs.service;

import com.google.gson.Gson;
import com.mashape.unirest.http.JsonNode;
import com.swipejobs.model.Worker;
import com.swipejobs.exceptions.ResourceNotFoundException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by sbhowmick on 2/28/17.
 */

@Component
public class WorkerService extends AbstractService {
    @Value("${swipejobs.baseUrl}")
    private String baseUrl;

    @Value("${swipejobs.workersApi}")
    private String workerApi;

    private Map<Integer, Worker> workers = new HashMap<>();

    @PostConstruct
    public void loadWorkers() {
        final String workersUrl = baseUrl + workerApi;

        Gson gson = new Gson();
        JsonNode jsonNode = callRemoteService(workersUrl);
        JSONArray jsonArray = jsonNode.getArray();
        for (int index = 0; index < jsonArray.length(); index++) {
            JSONObject jsonObject = jsonArray.getJSONObject(index);
            Worker worker = gson.fromJson(jsonObject.toString(), Worker.class);
            workers.put(worker.userId, worker);
        }
    }

    public Worker findWorkerById(Integer id) {
        Worker worker = workers.get(id);
        if (worker != null) {
            return worker;
        }
        throw new ResourceNotFoundException(String.format("Worker with id %d does not exists", id));
    }
}
