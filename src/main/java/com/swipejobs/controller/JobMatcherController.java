package com.swipejobs.controller;

import com.swipejobs.model.Job;
import com.swipejobs.model.JobResult;
import com.swipejobs.model.Worker;
import com.swipejobs.service.JobMatchingService;
import com.swipejobs.service.JobService;
import com.swipejobs.service.WorkerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by sbhowmick on 2/28/17.
 */
@RestController
@RequestMapping(value = "jobmatcher/v1")
public class JobMatcherController extends AbstractController {

    @Autowired
    private JobMatchingService jobMatchingService;

    @Autowired
    private WorkerService workerService;

    @Autowired
    private JobService jobService;

    @RequestMapping(value = "worker/match/{workerId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<JobResult> match(@PathVariable Integer workerId) {
        return ResponseEntity.ok(jobMatchingService.matchJobsForWorker(workerId));
    }

    @RequestMapping(value = "job/{jobId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Job> job(@PathVariable Integer jobId) {
        return ResponseEntity.ok(jobService.findJobById(jobId));
    }

    @RequestMapping(value = "worker/{workerId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Worker> worker(@PathVariable Integer workerId) {
        return ResponseEntity.ok(workerService.findWorkerById(workerId));
    }
}
