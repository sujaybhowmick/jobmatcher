package com.swipejobs.model;

import java.util.List;

/**
 * Created by sbhowmick on 2/28/17.
 */
public class JobResult {
    public Worker worker;

    public List<Job> jobs;

    public int matches;


    public JobResult(Worker worker, List<Job> jobs) {
        this.worker = worker;
        this.jobs = jobs;
        this.matches = jobs.size();
    }
}
