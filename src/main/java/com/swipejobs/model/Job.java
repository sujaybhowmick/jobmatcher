
package com.swipejobs.model;

import java.util.List;

public class Job {

    public Boolean driverLicenseRequired;

    public List<String> requiredCertificates = null;

    public Location location;

    public String billRate;

    public Integer workersRequired;

    public String startDate;

    public String about;

    public String jobTitle;

    public String company;

    public String guid;

    public Integer jobId;

    public Job withDriverLicenseRequired(Boolean driverLicenseRequired) {
        this.driverLicenseRequired = driverLicenseRequired;
        return this;
    }

    public Job withRequiredCertificates(List<String> requiredCertificates) {
        this.requiredCertificates = requiredCertificates;
        return this;
    }

    public Job withLocation(Location location) {
        this.location = location;
        return this;
    }

    public Job withBillRate(String billRate) {
        this.billRate = billRate;
        return this;
    }

    public Job withWorkersRequired(Integer workersRequired) {
        this.workersRequired = workersRequired;
        return this;
    }

    public Job withStartDate(String startDate) {
        this.startDate = startDate;
        return this;
    }

    public Job withAbout(String about) {
        this.about = about;
        return this;
    }

    public Job withJobTitle(String jobTitle) {
        this.jobTitle = jobTitle;
        return this;
    }

    public Job withCompany(String company) {
        this.company = company;
        return this;
    }

    public Job withGuid(String guid) {
        this.guid = guid;
        return this;
    }

    public Job withJobId(Integer jobId) {
        this.jobId = jobId;
        return this;
    }

}
