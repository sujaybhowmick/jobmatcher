
package com.swipejobs.model;

public class Location {


    public String longitude;

    public String latitude;

    public Location withLongitude(String longitude) {
        this.longitude = longitude;
        return this;
    }

    public Location withLatitude(String latitude) {
        this.latitude = latitude;
        return this;
    }

    public double distance(Location toLocation, String unit) {
        double lon1 = Double.parseDouble(this.longitude);
        double lon2 = Double.parseDouble(toLocation.longitude);
        double lat1 = Double.parseDouble(this.latitude);
        double lat2 = Double.parseDouble(toLocation.latitude);
        double theta = lon1 - lon2;
        double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == "K") {
            dist = dist * 1.609344;
        } else if (unit == "N") {
            dist = dist * 0.8684;
        }

        return (dist);
    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }

    private static double rad2deg(double rad) {
        return (rad * 180 / Math.PI);
    }
}
