
package com.swipejobs.model;

public class JobSearchAddress {

    public String unit;


    public Integer maxJobDistance;


    public String latitude;

    public String longitude;

    public JobSearchAddress withUnit(String unit) {
        this.unit = unit;
        return this;
    }

    public JobSearchAddress withMaxJobDistance(Integer maxJobDistance) {
        this.maxJobDistance = maxJobDistance;
        return this;
    }

}
