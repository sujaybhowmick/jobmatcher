
package com.swipejobs.model;

import java.util.List;

public class Worker {

    public Integer rating;

    public Boolean isActive;

    public List<String> certificates = null;

    public List<String> skills = null;

    public JobSearchAddress jobSearchAddress;

    public String transportation;

    public Boolean hasDriversLicense;

    public List<Availability> availability = null;

    public String phone;

    public String email;

    public Name name;

    public Integer age;

    public String guid;

    public Integer userId;

    public Worker withRating(Integer rating) {
        this.rating = rating;
        return this;
    }

    public Worker withIsActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public Worker withCertificates(List<String> certificates) {
        this.certificates = certificates;
        return this;
    }

    public Worker withSkills(List<String> skills) {
        this.skills = skills;
        return this;
    }

    public Worker withJobSearchAddress(JobSearchAddress jobSearchAddress) {
        this.jobSearchAddress = jobSearchAddress;
        return this;
    }

    public Worker withTransportation(String transportation) {
        this.transportation = transportation;
        return this;
    }

    public Worker withHasDriversLicense(Boolean hasDriversLicense) {
        this.hasDriversLicense = hasDriversLicense;
        return this;
    }

    public Worker withAvailability(List<Availability> availability) {
        this.availability = availability;
        return this;
    }

    public Worker withPhone(String phone) {
        this.phone = phone;
        return this;
    }

    public Worker withEmail(String email) {
        this.email = email;
        return this;
    }

    public Worker withName(Name name) {
        this.name = name;
        return this;
    }

    public Worker withAge(Integer age) {
        this.age = age;
        return this;
    }

    public Worker withGuid(String guid) {
        this.guid = guid;
        return this;
    }

    public Worker withUserId(Integer userId) {
        this.userId = userId;
        return this;
    }

}
