
package com.swipejobs.model;

public class Availability {

    public String title;

    public Integer dayIndex;

    public Availability withTitle(String title) {
        this.title = title;
        return this;
    }

    public Availability withDayIndex(Integer dayIndex) {
        this.dayIndex = dayIndex;
        return this;
    }

}
