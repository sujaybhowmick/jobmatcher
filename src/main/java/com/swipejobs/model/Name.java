
package com.swipejobs.model;

public class Name {

    public String last;

    public String first;

    public Name withLast(String last) {
        this.last = last;
        return this;
    }

    public Name withFirst(String first) {
        this.first = first;
        return this;
    }

}
