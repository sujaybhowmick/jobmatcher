package com.swipejobs.exceptions;

/**
 * Created by sbhowmick on 3/1/17.
 */
public class RestServiceCallException extends RuntimeException {
    public RestServiceCallException(String message) {
        super(message);
    }
}
