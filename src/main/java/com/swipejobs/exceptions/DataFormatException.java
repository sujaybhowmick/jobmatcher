package com.swipejobs.exceptions;

/**
 * Created by sbhowmick on 3/1/17.
 */
public class DataFormatException extends RuntimeException {
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    public DataFormatException(Throwable t) {
        super(t);
    }

    public DataFormatException() {
        super();
    }

    public DataFormatException(String message, Throwable cause) {
        super(message, cause);
    }

    public DataFormatException(String message) {
        super(message);
    }
}
