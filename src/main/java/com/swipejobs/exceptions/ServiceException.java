package com.swipejobs.exceptions;

/**
 * Created by sbhowmick on 3/1/17.
 */
public class ServiceException extends RuntimeException {

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
