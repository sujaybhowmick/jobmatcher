package com.swipejobs.exceptions;

/**
 * Created by sbhowmick on 3/1/17.
 */
public class ServiceTimedOutException extends RuntimeException {

    public ServiceTimedOutException(Throwable cause) {
        super(cause);
    }
}
